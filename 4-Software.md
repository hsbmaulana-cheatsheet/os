<center>
<table>
<tbody>

<tr>
<td align="center" width="9999">-</td>
<td align="center" width="9999"><b>Package Manager</b></td>
<td align="center" width="9999">-</td>
</tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>
<tr>
<td align="center" width="9999">apt-cache search ...</td>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">snap search ...</td>
</tr>
<tr>
<td align="center" width="9999">apt list</td>
<td align="center" width="9999">dpkg -l</td>
<td align="center" width="9999">snap list</td>
</tr>
<tr>
<td align="center" width="9999">apt install -y ...</td>
<td align="center" width="9999">dpkg -i *.deb</td>
<td align="center" width="9999">snap install --classic ...</td>
</tr>
<tr>
<td align="center" width="9999">apt remove ...</td>
<td align="center" width="9999">dpkg -r ...</td>
<td align="center" width="9999">snap remove ...</td>
</tr>
<tr>
<td align="center" width="9999">apt purge ...</td>
<td align="center" width="9999">dpkg -P ...</td>
<td align="center" width="9999">snap remove --purge ...</td>
</tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>

<tr>
<td align="center" width="9999">-</td>
<td align="center" width="9999"><b>Process&Thread</b></td>
<td align="center" width="9999">-</td>
</tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>
<tr>
<td align="center" width="9999">ps -eTf</td>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">ps -eTo ppid,pid,<br/>cmd,comm</td>
</tr>
<tr>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">ps -u user, ps -g group</td>
<td align="center" width="9999">&nbsp;</td>
</tr>
<tr>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">ps --pid 0000</td>
<td align="center" width="9999">&nbsp;</td>
</tr>
<tr>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">&nbsp;</td>
</tr>
<tr>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">top</td>
<td align="center" width="9999">&nbsp;</td>
</tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>

<tr>
<td align="center" width="9999">-</td>
<td align="center" width="9999"><b>Service</b></td>
<td align="center" width="9999">-</td>
</tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>
<tr>
<td align="center" width="9999">SysVInit<br/>(/etc/init.d/)</td>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">Systemd<br/>(/lib/systemd/system/)</td>
</tr>
<tr>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">systemctl list-units<br/>--type=mount/device/socket/service</td>
<td align="center" width="9999">&nbsp;</td>
</tr>
<tr>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">systemctl status ...</td>
<td align="center" width="9999">&nbsp;</td>
</tr>
<tr>
<td align="center" width="9999">systemctl start ...</td>
<td align="center" width="9999">systemctl restart ...</td>
<td align="center" width="9999">systemctl stop ...</td>
</tr>
<tr>
<td align="center" width="9999">systemctl enable ...</td>
<td align="center" width="9999">systemctl reenable ...</td>
<td align="center" width="9999">systemctl disable ...</td>
</tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>

<tr>
<td align="center" width="9999">-</td>
<td align="center" width="9999"><b>Daemon</b></td>
<td align="center" width="9999">-</td>
</tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>
<tr>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">
<a href="https://github.com/jirihnidek/daemon">Simple</a>
&
<a href="https://github.com/pasce/daemon-skeleton-linux-c">Expert</a>
</td>
<td align="center" width="9999">&nbsp;</td>
</tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>

<tr>
<td align="center" width="9999">-</td>
<td align="center" width="9999"><b>Runlevel</b></td>
<td align="center" width="9999">-</td>
</tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>
<tr>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">init 1</td>
<td align="center" width="9999">&nbsp;</td>
</tr>
<tr>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">init 2, 3, 4</td>
<td align="center" width="9999">&nbsp;</td>
</tr>
<tr>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">init 5</td>
<td align="center" width="9999">&nbsp;</td>
</tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>

<tr>
<td align="center" width="9999">-</td>
<td align="center" width="9999"><b>Kill</b></td>
<td align="center" width="9999">-</td>
</tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>
<tr>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">
kill <a href="https://www.javatpoint.com/linux-signals">-SIGNAL</a> 0000
</td>
<td align="center" width="9999">&nbsp;</td>
</tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>

</tbody>
</table>
</center>
