<center>
<table>
<tbody>

<tr>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999"><div><span>POSIX</span></div><div><span>(Kernel Standard)</div></td>
<td align="center" width="9999">&nbsp;</td>
</tr>

<tr>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999"><span>Unix, Linux, BSD, Darwin, NT</span></td>
<td align="center" width="9999">&nbsp;</td>
</tr>

<tr>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">&nbsp;</td>
</tr>

<tr>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999"><span>GNU</span></td>
<td align="center" width="9999">&nbsp;</td>
</tr>

<tr>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">&nbsp;</td>
</tr>

<tr>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999"><span>GUI</span></td>
<td align="center" width="9999">&nbsp;</td>
</tr>

<tr>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999"><span>CLI<br/>(console, terminal, tty, repl)</span></td>
<td align="center" width="9999">&nbsp;</td>
</tr>

<tr>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">&nbsp;</td>
</tr>

<tr>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999"><span title="The place for portable software...">/opt/</span></td>
<td align="center" width="9999">&nbsp;</td>
</tr>

<tr>
<td align="center" width="9999"><span title="Software to handle hardware...">/bin/</span></td>
<td align="center" width="9999"><span title="Software will be available in all runlevel include single user mode...">1</span></td>
<td align="center" width="9999"><span title="Software to handle configuration...">/sbin/</span></td>
</tr>

<tr>
<td align="center" width="9999"><span>/usr/bin/</span></td>
<td align="center" width="9999"><span title="Software will be available from runlevel multi user mode and installed by package manager...">2</span></td>
<td align="center" width="9999"><span>/usr/sbin/</span></td>
</tr>

<tr>
<td align="center" width="9999"><span>/usr/local/bin/</span></td>
<td align="center" width="9999"><span title="Software will be available from runlevel multi user mode and installed by manual user...">3</span></td>
<td align="center" width="9999"><span>/usr/local/sbin/</span></td>
</tr>

<tr>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">&nbsp;</td>
</tr>

<tr>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999"><span>STDIN (0), STDOUT (1), STDERR (2)<br/><br/>&gt;, &lt;<br/><br/>&gt;&gt;, &lt;&lt;EOF ... EOF<br/><br/>... &<br/>... | ...<br/><br/>... && ...<br/>... || ...</span></td>
<td align="center" width="9999">&nbsp;</td>
</tr>

</tbody>
</table>
</center>
