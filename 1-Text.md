<center>
<table>
<tbody>

<tr>
<td align="center" width="9999">-</td>
<td align="center" width="9999"><b>Copy Paste</b></td>
<td align="center" width="9999">-</td>
</tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>
<tr><td align="center" width="9999" colspan="3">cat file.txt | xclip -selection clipboard -i</td></tr>
<tr><td align="center" width="9999" colspan="3">cat file.txt | xclip -selection clipboard -o</td></tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>

<tr>
<td align="center" width="9999">-</td>
<td align="center" width="9999"><b>Search File</b></td>
<td align="center" width="9999">-</td>
</tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>
<tr><td align="center" width="9999" colspan="3">find . -type d|f|l -iname '*.txt' -exec ... {} \;</td></tr>
<tr><td align="center" width="9999" colspan="3">find . -type d|f|l -iname '???.txt' -exec ... {} +</td></tr>
<tr><td align="center" width="9999" colspan="3">find . -type d|f|l -perm 777</td></tr>
<tr><td align="center" width="9999" colspan="3">find . -type d|f|l -size -2M</td></tr>
<tr><td align="center" width="9999" colspan="3">find . -type d|f|l -size +2M</td></tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>
<tr><td align="center" width="9999" colspan="3">find . -perm 777 -and -size -+2M</td></tr>
<tr><td align="center" width="9999" colspan="3">find . -perm 777 -or -size -+2M</td></tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>

<tr>
<td align="center" width="9999">-</td>
<td align="center" width="9999"><b>Search Text Inside File</b></td>
<td align="center" width="9999">-</td>
</tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>
<tr><td align="center" width="9999" colspan="3">fgrep -i 'abc' . -r -n</td></tr>
<tr><td align="center" width="9999" colspan="3">grep -i '[] \| \() \{} \* \+ \?' . -r -n</td></tr>
<tr><td align="center" width="9999" colspan="3">egrep -i '[] | () {} * + ?' . -r -n</td></tr>
<tr>
<td align="center" width="9999">grep -P -i '(Hello)\s(?=World)' . -r</td>
<td align="center" width="9999"><b>Look Ahead</b></td>
<td align="center" width="9999">grep -P -i '(Hello)\s(?!World)' . -r</td>
</tr>
<tr>
<td align="center" width="9999">grep -P -i '(?&lt;=Hello)\s(World)' . -r</td>
<td align="center" width="9999"><b>Look Behind</b></td>
<td align="center" width="9999">grep -P -i '(?&lt;!Hello)\s(World)' . -r</td>
</tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>

<tr>
<td align="center" width="9999">-</td>
<td align="center" width="9999"><b>Process Row&Column Filetext</b></td>
<td align="center" width="9999">-</td>
</tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>
<tr><td align="center" width="9999" colspan="3">awk '{variable=$1 " XY " $2; print variable}'</td></tr>
<tr><td align="center" width="9999" colspan="3">awk 'BEGIN{print "Header"} {print $0} END{print "Footer"}'</td></tr>
<tr><td align="center" width="9999" colspan="3">awk '{print $0,"XY",$NF,NF,NR}'</td></tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>
<tr><td align="center" width="9999" colspan="3">awk '$1 ~ /^$/ {print $0}'</td></tr>
<tr><td align="center" width="9999" colspan="3">awk '$1 !~ /^$/ {print $0}'</td></tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>

<tr>
<td align="center" width="9999">-</td>
<td align="center" width="9999"><b>Editor Filetext</b></td>
<td align="center" width="9999">-</td>
</tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>
<tr><td align="center" width="9999" colspan="3"><b>Alter Text</b></td></tr>
<tr><td align="center" width="9999" colspan="3">sed -i '3,$ s/^searchtext$/replacetext/i' file.txt</td></tr>
<tr><td align="center" width="9999" colspan="3">sed -i 's/^searchtext$/replacetext/ig' file.txt</td></tr>
<tr><td align="center" width="9999" colspan="3"><b>Remove Line</b></td></tr>
<tr><td align="center" width="9999" colspan="3">sed -i '3,$d' file.txt</td></tr>
<tr><td align="center" width="9999" colspan="3">sed -i '/^searchtext$/d' file.txt</td></tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>

</tbody>
</table>
</center>
