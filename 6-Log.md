<center>
<table>
<tbody>

<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>
<tr>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">last -xiF -n5</td>
<td align="center" width="9999">&nbsp;</td>
</tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>

<tr>
<td align="center" width="9999">-</td>
<td align="center" width="9999"><b>Livelog</b></td>
<td align="center" width="9999">-</td>
</tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>
<tr>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">journalctl -f</td>
<td align="center" width="9999">&nbsp;</td>
</tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>

<tr>
<td align="center" width="9999">-</td>
<td align="center" width="9999"><b>Deadlog</b></td>
<td align="center" width="9999">-</td>
</tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>
<tr>
<td align="center" width="9999">journalctl -xr -b0</td>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">journalctl -xr -u ...</td>
</tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>
<tr>
<td align="center" width="9999">--since "n ago"</td>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">--until "n ago"</td>
</tr>
<tr>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">--priority "emerg|alert|crit|err|warning|notice|info|debug"</td>
<td align="center" width="9999">&nbsp;</td>
</tr>
<tr>
<td align="center" width="9999">_UID=$(id -u)</td>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">_GID=$(id -g)</td>
</tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>

<tr>
<td align="center" width="9999">-</td>
<td align="center" width="9999"><b>Log to Unlog</b></td>
<td align="center" width="9999">-</td>
</tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>
<tr>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">journalctl --disk-usage</td>
<td align="center" width="9999">&nbsp;</td>
</tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>
<tr>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">journalctl --flush --rotate</td>
<td align="center" width="9999">&nbsp;</td>
</tr>
<tr>
<td align="center" width="9999">journalctl -m<br/>--vacuum-files=0</td>
<td align="center" width="9999">journalctl -m<br/>--vacuum-time=0<br/>days|months|weeks|years</td>
<td align="center" width="9999">journalctl -m<br/>--vacuum-size=0<br/>K|M|G</td>
</tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>

</tbody>
</table>
</center>
