<center>
<table>
<tbody>

<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>
<tr>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">id $(whoami)</td>
<td align="center" width="9999">&nbsp;</td>
</tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>

<tr>
<td align="center" width="9999">-</td>
<td align="center" width="9999"><b>Group</b></td>
<td align="center" width="9999">-</td>
</tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>
<tr>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">/etc/group</td>
<td align="center" width="9999">&nbsp;</td>
</tr>
<tr>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">groupadd ...</td>
<td align="center" width="9999">&nbsp;</td>
</tr>
<tr>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">groupdel ...</td>
<td align="center" width="9999">&nbsp;</td>
</tr>
<tr>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">groupmod <i>option</i> ...</td>
<td align="center" width="9999">&nbsp;</td>
</tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>

<tr>
<td align="center" width="9999">-</td>
<td align="center" width="9999"><b>User</b></td>
<td align="center" width="9999">-</td>
</tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>
<tr>
<td align="center" width="9999">/etc/passwd</td>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">/etc/shadow</td>
</tr>
<tr>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">useradd -m -N -g ... -G ...,...,... ...<br/>&&<br/>passwd ...</td>
<td align="center" width="9999">&nbsp;</td>
</tr>
<tr>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">userdel -r ...</td>
<td align="center" width="9999">&nbsp;</td>
</tr>
<tr>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">usermod <i>option</i> ...</td>
<td align="center" width="9999">&nbsp;</td>
</tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>

<tr>
<td align="center" width="9999">sudo</td>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">su -</td>
</tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>

</tbody>
</table>
</center>
