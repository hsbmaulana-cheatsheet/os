<center>
<table>
<tbody>

<tr>
<td align="center" width="9999">-</td>
<td align="center" width="9999"><b>File&Folder</b></td>
<td align="center" width="9999">-</td>
</tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>
<tr><td align="center" width="9999">file --mime file.txt</td><td align="center" width="9999"><a href="https://en.wikipedia.org/wiki/Unix_file_types">stat ...</a></td><td align="center" width="9999">file --special-files /dev/sda3</td></tr>
<tr><td align="center" width="9999">ls -lt</td><td>&nbsp;</td><td align="center" width="9999">ls -lS</td></tr>
<tr><td>&nbsp;</td><td align="center" width="9999">du -hcd 1</td><td>&nbsp;</td></tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>

<tr>
<td align="center" width="9999">-</td>
<td align="center" width="9999"><b>Permission</b></td>
<td align="center" width="9999">-</td>
</tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>
<tr><td align="center" width="9999">chown -R user:group .<br/>chgrp -R group .</td><td>&nbsp;</td><td align="center" width="9999">chmod -R <a href="http://www.javascriptkit.com/script/script2/chmodcal2.shtml">0</a>421 .<br/>chmod -R augo+-=rwx<a href="https://linuxhint.com/setuid-setgid-sticky-bit">st</a> .</td></tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>

<tr>
<td align="center" width="9999">-</td>
<td align="center" width="9999"><b>Capability</b></td>
<td align="center" width="9999">-</td>
</tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>
<tr><td>&nbsp;</td><td align="center" width="9999">capsh --print</td><td>&nbsp;</td></tr>
<tr><td>&nbsp;</td><td align="center" width="9999">setcap cap_...,cap_...,cap_...+/-epi file</td><td>&nbsp;</td></tr>
<tr><td>&nbsp;</td><td align="center" width="9999">getcap file</td><td>&nbsp;</td></tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>

<tr>
<td align="center" width="9999">-</td>
<td align="center" width="9999"><b>Metadata</b></td>
<td align="center" width="9999">-</td>
</tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>
<tr><td>&nbsp;</td><td align="center" width="9999">xattr -w x.y value file</td><td>&nbsp;</td></tr>
<tr><td>&nbsp;</td><td align="center" width="9999">xattr -g x.y file</td><td>&nbsp;</td></tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>

<tr>
<td align="center" width="9999">-</td>
<td align="center" width="9999"><b>Soft&Hard</b></td>
<td align="center" width="9999">-</td>
</tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>
<tr><td align="center" width="9999">ln -s /target/ /softlink/</td><td>&nbsp;</td><td align="center" width="9999">ln /target/ /hardlink/</td></tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>

<tr>
<td align="center" width="9999">-</td>
<td align="center" width="9999"><b>Backup&Restore</b></td>
<td align="center" width="9999">-</td>
</tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>
<tr><td>&nbsp;</td><td align="center" width="9999">rsync -razvP from/ to/</td><td>&nbsp;</td></tr>
<tr><td align="center" width="9999"><i>only see progress not transfer</i></td><td>&nbsp;</td><td align="center" width="9999">rsync -razvP --dry-run from/ to/</td></tr>
<tr><td align="center" width="9999"><i>clean destination then transfer</i></td><td>&nbsp;</td><td align="center" width="9999">rsync -razvP --delete from/ to/</td></tr>
<tr><td align="center" width="9999"><i>transfer only newer source</i></td><td>&nbsp;</td><td align="center" width="9999">rsync -razvP --ignore-existing from/ to/</td></tr>
<tr><td align="center" width="9999"><i>transfer only updated source<i></td><td>&nbsp;</td><td align="center" width="9999">rsync -razvP --update from/ to/</td></tr>
<tr><td align="center" width="9999"><i>local to remote</i></td><td>&nbsp;</td><td align="center" width="9999">rsync -razvP -e 'ssh -i .key' /from/ user@host:/to/</td></tr>
<tr><td align="center" width="9999"><i>remote to local</i></td><td>&nbsp;</td><td align="center" width="9999">rsync -razvP -e 'ssh -i .key' user@host:/from/ /to/</td></tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>

<tr>
<td align="center" width="9999">-</td>
<td align="center" width="9999"><b>Burn</b></td>
<td align="center" width="9999">-</td>
</tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>
<tr><td>&nbsp;</td><td align="center" width="9999">dd if=file.iso of=/dev/sda3 conv=noerror,sync status=progress</td><td>&nbsp;</td></tr>
<tr><td>&nbsp;</td><td align="center" width="9999">dd if=file.iso of=/dev/sda3 conv=noerror status=progress</td><td>&nbsp;</td></tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>

<tr>
<td align="center" width="9999">-</td>
<td align="center" width="9999"><b>Disk Manage</b></td>
<td align="center" width="9999">-</td>
</tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>
<tr><td align="center" width="9999">parted -s /dev/sda print all|free</td><td align="center" width="9999">&nbsp;</td><td align="center" width="9999">findmnt</td></tr>
<tr><td>&nbsp;</td><td align="center" width="9999">mount --bind -t ext4|ntfs|vfat -o sync|async,ro|rw,loop /dev/sda3 /folder/</td><td>&nbsp;</td></tr>
<tr><td>&nbsp;</td><td align="center" width="9999">mount --rbind -t ext4|ntfs|vfat -o sync|async,ro|rw,loop /dev/sda3 /folder/</td><td>&nbsp;</td></tr>
<tr><td>&nbsp;</td><td align="center" width="9999">mount --move -t ext4|ntfs|vfat -o sync|async,ro|rw,loop /dev/sda3 /folder/</td><td>&nbsp;</td></tr>
<tr><td>&nbsp;</td><td align="center" width="9999">umount /dev/sda3</td><td>&nbsp;</td></tr>
<tr><td>&nbsp;</td><td align="center" width="9999">eject /dev/sda</td><td>&nbsp;</td></tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>
<tr><td>&nbsp;</td><td align="center" width="9999">Create Device Standard</td><td>&nbsp;</td></tr>
<tr><td align="center" width="9999">(MBR)</td><td>&nbsp;</td><td align="center" width="9999">(GPT)</td></tr>
<tr><td align="center" width="9999">parted -s /dev/sda mklabel msdos</td><td>&nbsp;</td><td align="center" width="9999">parted -s /dev/sda mklabel gpt</td></tr>
<tr><td>&nbsp;</td><td align="center" width="9999">Create Partition</td><td>&nbsp;</td></tr>
<tr><td>&nbsp;</td><td align="center" width="9999">parted -s /dev/sda mkpart primary|extended|logical ext4|ntfs|fat32|linux-swap 0M 128M</td><td>&nbsp;</td></tr>
<tr><td>&nbsp;</td><td align="center" width="9999">parted -s /dev/sda set 3 flag on|off</td><td>&nbsp;</td></tr>
<tr><td>&nbsp;</td><td align="center" width="9999">Format Partition</td><td>&nbsp;</td></tr>
<tr><td align="center" width="9999">mkfs.ext4 -L "xy" /dev/sda3</td><td align="center" width="9999">mkfs.ntfs -L "xy" /dev/sda3</td><td align="center" width="9999">mkfs.vfat -L "xy" /dev/sda3</td></tr>
<tr><td>&nbsp;</td><td align="center" width="9999">Delete Partition</td><td>&nbsp;</td></tr>
<tr><td>&nbsp;</td><td align="center" width="9999">parted -s /dev/sda rm 3</td><td>&nbsp;</td></tr>
<tr><td align="center" width="9999">Extend Partition</td><td>&nbsp;</td><td align="center" width="9999">Shrink Partition</td></tr>
<tr><td align="center" width="9999">parted -s /dev/sda resizepart 3 512M</td><td>&nbsp;</td><td align="center" width="9999">parted -s /dev/sda resizepart 3 256M</td></tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>

</tbody>
</table>
</center>
