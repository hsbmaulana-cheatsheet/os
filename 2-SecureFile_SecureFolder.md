<center>
<table>
<tbody>

<tr>
<td align="center" width="9999"><b>Encoding</b></td>
<td align="center" width="9999">-</td>
<td align="center" width="9999"><b>Decoding</b></td>
</tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>
<tr><td align="center" width="9999">base32 &lt;(echo "32")</td><td>&nbsp;</td><td align="center" width="9999">base32 -d &lt;(echo "XXXX")</td></tr>
<tr><td align="center" width="9999">base64 &lt;(echo "64")</td><td>&nbsp;</td><td align="center" width="9999">base64 -d &lt;(echo "YYYY")</td></tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>

<tr>
<td align="center" width="9999"><b>Archieve</b></td>
<td align="center" width="9999">-</td>
<td align="center" width="9999"><b>Unarchieve</b></td>
</tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>
<tr><td align="center" width="9999">&nbsp;</td><td align="center" width="9999">tar -tvf file.tar</td><td align="center" width="9999">&nbsp;</td></tr>
<tr><td align="center" width="9999">tar -cvf file.tar folder folder</td><td>&nbsp;</td><td align="center" width="9999">tar -xvf file.tar --one-top-level</td></tr>
<tr><td align="center" width="9999">&nbsp;</td><td align="center" width="9999">unzip -l file.zip</td><td align="center" width="9999">&nbsp;</td></tr>
<tr><td align="center" width="9999">zip -r file.zip folder folder</td><td>&nbsp;</td><td align="center" width="9999">unzip -d destination/ file.zip</td></tr>
<tr><td align="center" width="9999">&nbsp;</td><td align="center" width="9999">unrar l file.rar</td><td align="center" width="9999">&nbsp;</td></tr>
<tr><td align="center" width="9999">rar a file.rar folder folder</td><td>&nbsp;</td><td align="center" width="9999">unrar e file.rar destination/</td></tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>

<tr>
<td align="center" width="9999"><b>Compress</b></td>
<td align="center" width="9999">-</td>
<td align="center" width="9999"><b>Decompress</b></td>
</tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>
<tr><td align="center" width="9999">&nbsp;</td><td align="center" width="9999">tar -tjvf file.tar.bz2</td><td align="center" width="9999">&nbsp;</td></tr>
<tr><td align="center" width="9999">tar -cjvf file.tar.bz2 folder folder</td><td>&nbsp;</td><td align="center" width="9999">tar -xjvf file.tar.bz2 --one-top-level</td></tr>
<tr><td align="center" width="9999">&nbsp;</td><td align="center" width="9999">tar -tzvf file.tar.gz</td><td align="center" width="9999">&nbsp;</td></tr>
<tr><td align="center" width="9999">tar -czvf file.tar.gz folder folder</td><td>&nbsp;</td><td align="center" width="9999">tar -xzvf file.tar.gz --one-top-level</td></tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>

<tr>
<td align="center" width="9999"><b>Encryption</b></td>
<td align="center" width="9999">-</td>
<td align="center" width="9999"><b>Decryption</b></td>
</tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>
<tr><td align="center" width="9999">&nbsp;</td><td align="center" width="9999">Symmetric</td><td align="center" width="9999">&nbsp;</td></tr>
<tr><td align="center" width="9999" colspan="3">gpg -o chiper.gpg --symmetric --sign plain.txt</td></tr>
<tr><td align="center" width="9999" colspan="3">gpg -o chiper.asc --symmetric --sign -a plain.txt</td></tr>
<tr><td align="center" width="9999" colspan="3">gpg -o newplain -d chiper && file --mime newplain</td></tr>
<tr><td align="center" width="9999">&nbsp;</td><td align="center" width="9999">Asymmetric</td><td align="center" width="9999">&nbsp;</td></tr>
<tr><td align="center" width="9999" colspan="3">gpg -o chiper.gpg --encrypt -r user@email.com --sign plain.txt</td></tr>
<tr><td align="center" width="9999" colspan="3">gpg -o chiper.asc --encrypt -r user@email.com --sign -a plain.txt</td></tr>
<tr><td align="center" width="9999" colspan="3">gpg -o newplain -d chiper && file --mime newplain</td></tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>

<tr>
<td align="center" width="9999">-</td>
<td align="center" width="9999"><b>Checksum</b></td>
<td align="center" width="9999">-</td>
</tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>
<tr><td align="center" width="9999" colspan="3">sha256sum fle.iso > sum.txt</td></tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>

<tr>
<td align="center" width="9999"><b>Sign</b></td>
<td align="center" width="9999">-</td>
<td align="center" width="9999"><b>Unsign</b></td>
</tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>
<tr><td align="center" width="9999">gpg -o signed.sig --sign -a plain.txt</td><td>&nbsp;</td><td align="center" width="9999">gpg -o newplain -d signed.sig</td></tr>
<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>

</tbody>
</table>
</center>
